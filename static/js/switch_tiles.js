window.addEventListener('load', registerEvents, false)

function registerEvents (event) {
  // console.log('dans registerEvents')
  // var parser = document.createElement('a')
  // parser.href = this.href
  // var cat = parser.pathname.split('/')[1]
  var navbarActiveEntry = document.querySelector('.active.navbar_entry')
  navbarActiveEntry.addEventListener('click', displayCategoryContent, false)
  navbarActiveEntry.addEventListener('click', turnOffAccordion, false)
  navbarActiveEntry.addEventListener('click', turnOffIPSelector, false)

  var leftentriesList = document.querySelectorAll('a.leftentry')
  for (var i = 0; i < leftentriesList.length; ++i) {
    // console.log('dans registerEvents: ' + i)
    if (leftentriesList[i].classList.contains('accordion_button')) {
      leftentriesList[i].addEventListener('click', turnOnAccordion, false)
      leftentriesList[i].addEventListener('click', turnOffIPSelector, false)
    } else {
      leftentriesList[i].addEventListener('click', displayTiles, false)
      if (!leftentriesList[i].classList.contains('belt')) {
        leftentriesList[i].addEventListener('click', turnOffAccordion, false)
        leftentriesList[i].addEventListener('click', turnOffIPSelector, false)
      } else {
        leftentriesList[i].addEventListener('click', turnOnIPSelector, false)
      }
    }
  }

  // var accordionButton = document.querySelector('.accordion_button')
  // accordionButton.addEventListener('click', turnOnAccordion, false)
  // document.getElementById('leftmenu_4e_equations').addEventListener('click', displayTiles, false)
}

function fadeOut (el) {
  el.style.opacity = 1

  ;(function fade () {
    var val = parseFloat(el.style.opacity)
    if (!((val += 0.1) > 1)) {
      el.style.opacity = val
      window.requestAnimationFrame(fade)
    }
  })()
}

function fadeIn (el, display) {
  el.style.opacity = 0
  el.style.display = display || 'flex'

  ;(function fade () {
    var val = parseFloat(el.style.opacity)
    // console.log('dans fade(in): ' + val)
    // console.log('dans fade(in): ' + !((val += 0.1) > 1))
    if (!((val += 0.1) > 1)) {
      el.style.opacity = val
      window.requestAnimationFrame(fade)
    }
  })()
}

function displayTiles (event) {
  // console.log('[displayTiles] ')
  event.preventDefault()
  // Create a link object that will be used in fact to parse the url
  var parser = document.createElement('a')
  var centralContent = document.getElementById('central_content')
  var allTilesGroups = document.getElementsByClassName('tiles_group')
  // "this" being an anchor (<a></a>), it has a href attribute
  // console.log('[displayTiles]: this.href = ' + this.href)
  parser.href = this.href
  var cat = parser.pathname.split('/')[1]
  var thm = parser.pathname.split('/')[2]
  var id = cat + '_' + thm
  var tilesGroupId = 'central_' + id
  var leftmenuActiveId = 'leftmenu_' + id
  var tilesGroup = document.getElementById(tilesGroupId)
  // console.log('[displayTiles]: leftmenuActiveId = ' + leftmenuActiveId)

  var allLeftmenuEntries = document.querySelectorAll('.leftentry.cat_' + cat)
  for (var i = 0; i < allLeftmenuEntries.length; ++i) {
    allLeftmenuEntries[i].classList.remove('active')
  }

  var leftmenuActiveEntry = document.getElementById(leftmenuActiveId)
  leftmenuActiveEntry.classList.add('active')
  leftmenuActiveEntry.blur()

  fadeOut(centralContent)
  for (i = 0; i < allTilesGroups.length; ++i) {
    allTilesGroups[i].style.display = 'none'
  }
  document.getElementById('main_content_' + cat).style.display = 'none'
  document.getElementById('WELCOME').classList.add('hidden')
  tilesGroup.style.display = 'flex'

  fadeIn(centralContent)
  // console.log('[displayTiles]: faded in ' + tilesGroupId)
  return false
}

function displayCategoryContent (event) {
  event.preventDefault()
  var parser = document.createElement('a')
  var centralContent = document.getElementById('central_content')
  var allTilesGroups = document.getElementsByClassName('tiles_group')
  parser.href = this.href
  var cat = parser.pathname.split('/')[1]

  // console.log('dans displayCategoryContent: ' + )
  var allLeftmenuEntries = document.querySelectorAll('.leftentry.cat_' + cat)
  for (var i = 0; i < allLeftmenuEntries.length; ++i) {
    allLeftmenuEntries[i].classList.remove('active')
  }

  fadeOut(centralContent)
  for (i = 0; i < allTilesGroups.length; ++i) {
    allTilesGroups[i].style.display = 'none'
  }
  document.querySelector('.active.navbar_entry > a').blur()
  document.getElementById('main_content_' + cat).style.display = 'flex'
  document.getElementById('WELCOME').classList.add('hidden')
  fadeIn(centralContent)
  return false
}

// function toggleAccordion (event) {
//   console.log('[toggleAccordion]')
//   var accordionBelts = document.querySelector('.accordion_belts')
//   accordionBelts.classList.toggle('hidden')
// }

function turnOnAccordion (event) {
  event.preventDefault()
  var parser = document.createElement('a')
  parser.href = this.href
  var cat = parser.pathname.split('/')[1]
  var allLeftmenuEntries = document.querySelectorAll('.leftentry.cat_' + cat)
  var centralContent = document.getElementById('central_content')
  var welcome = document.getElementById('WELCOME')
  var allTilesGroups = document.getElementsByClassName('tiles_group')
  for (var i = 0; i < allLeftmenuEntries.length; ++i) {
    allLeftmenuEntries[i].classList.remove('active')
  }
  // console.log('[turnOnAccordion]')
  var accordionBelts = document.querySelector('.accordion_belts')
  accordionBelts.classList.remove('hidden')
  this.classList.add('active')

  var accordionButton = document.querySelector('.accordion_button')
  accordionButton.classList.remove('closed')
  accordionButton.classList.add('open')

  fadeOut(centralContent)
  for (i = 0; i < allTilesGroups.length; ++i) {
    allTilesGroups[i].style.display = 'none'
  }
  document.getElementById('main_content_' + cat).style.display = 'none'
  document.querySelector('.active.navbar_entry > a').blur()
  if (welcome.classList.contains('hidden')) {
    welcome.classList.remove('hidden')
    fadeIn(welcome)
  }
}

function turnOffAccordion (event) {
  event.preventDefault()
  // console.log('[turnOffAccordion]')
  var accordionBelts = document.querySelector('.accordion_belts')
  accordionBelts.classList.add('hidden')
  document.getElementById('WELCOME').classList.add('hidden')
  var accordionButton = document.querySelector('.accordion_button')
  accordionButton.classList.remove('open')
  accordionButton.classList.add('closed')
}

function turnOffIPSelector (event) {
  event.preventDefault()
  // console.log('[turnOffIPSelector]')
  var ipSelector = document.getElementById('ipselector')
  fadeOut(ipSelector)
  ipSelector.classList.remove('show')
  ipSelector.classList.add('hide')
}

function turnOnIPSelector (event) {
  event.preventDefault()
  // console.log('[turnOnIPSelector]')
  var ipSelector = document.getElementById('ipselector')
  if (!ipSelector.classList.contains('show')) {
    fadeOut(ipSelector, 'ipselector')
    ipSelector.classList.remove('hide')
    ipSelector.classList.add('show')
    fadeIn(ipSelector)
  }
}
